


namespace WSSignalR.Models {

    public class ConfigurationModel {

        public string RedisConfig { get; set; }

        public string[] NSQDs { get; set; }
    }
}