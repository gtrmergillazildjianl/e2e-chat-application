using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.AspNetCore.SignalR;
using WSSignalR.Hubs;
using Microsoft.Extensions.Configuration;
using WSSignalR.Models;
using RedisStores;
using WSSignalR.Middlewares;
using System.Text.Json;
using Microsoft.AspNetCore.SignalR.StackExchangeRedis;

using StackExchange.Redis;

namespace WSSignalR
{
    public class Startup
    {

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }
        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddSignalR();

            services.AddSingleton<ConfigurationModel>((provider) =>
            {
                return new ConfigurationModel
                {
                    NSQDs = Configuration.GetSection("NSQDs").Get<string[]>(),
                    RedisConfig = Configuration.GetSection("RedisConfig").Get<string>()
                };
            });

            services.AddOptions<JsonHubProtocolOptions>().Configure(options =>
            {
                options.PayloadSerializerOptions = new JsonSerializerOptions
                {
                    PropertyNamingPolicy = JsonNamingPolicy.CamelCase,
                    DictionaryKeyPolicy = JsonNamingPolicy.CamelCase
                };
            });

            services.AddAuthentication(options =>
            {
                options.AddScheme<CustomCookieAuthenticationMiddleware>("session", "session");
                options.DefaultAuthenticateScheme = "session";
            });
            services.AddRedisChannelStore(Configuration.GetSection("RedisConfig").Get<string>());
            
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            app.UseHealthMiddleware();
            app.UseRouting();
            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapHub<ChatHub>("/hub/chat").RequireAuthorization();
            });


        }
    }
}
