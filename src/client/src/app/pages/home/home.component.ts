import { Component, ContentChildren, OnInit, QueryList } from '@angular/core';
import { ChatService } from "../../services/chat.service";
import { faUser } from "@fortawesome/free-solid-svg-icons";
import { Router } from "@angular/router"


@Component({
  selector: 'home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  @ContentChildren(HTMLElement)
  children!: QueryList<HTMLElement>;
  
  faUser = faUser
  private password = ""
  private challenge = ""

  constructor(
    private chatService: ChatService,
    private router: Router,
  ) { }

  ngOnInit(): void {
  }

  async onCreateNewChannel(usernameInput: HTMLInputElement) {
    this.password = this.chatService.randomString(24);
    let channel = await this.chatService.CreateNewChannel(this.password).toPromise()
    if (channel.statusCode !== 201) {
      alert(`error occured: ${channel.statusCode}`)
      return;
    }
    await this.chatService.JoinChannel(usernameInput.value, channel.data?.channelID as string, this.password)
    this.chatService.CreateMessage("created channel").toPromise()
    this.router.navigate(['/chat'])
    
  }



}
