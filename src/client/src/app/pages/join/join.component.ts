import { Component } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router"
import { faUser, faLock } from "@fortawesome/free-solid-svg-icons"
import { ChatService } from "../../services/chat.service"
import { Channel } from "../../models/Channel"


@Component({
    selector: "join",
    templateUrl: "./join.component.html",
    styleUrls: ["./join.component.scss"]
})

export class JoinComponent {

    channel!: Channel
    faUser = faUser;
    faLock = faLock;
    
    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private chatService: ChatService
    ){}

    async ngOnInit() {
        let channelID = this.route.snapshot.paramMap.get("channelID") as string
        let result = await this.chatService.GetChannel(channelID).toPromise()
        if (result.statusCode !== 200) {
            this.router.navigateByUrl('/')
            return
        }
        this.channel = result.data as Channel
    }

    async onClickJoin(username: string, password: string) {
        console.log(username + password)
        try {
            await this.chatService.JoinChannel(username, this.channel.channelID as string, password)
            
            this.router.navigateByUrl('/chat')
        } catch {
            alert("Failed to join")
        }
        
    }
}