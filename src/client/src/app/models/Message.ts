export class Message {
    encryptedMessage?: string
    fourth?: string
    from?: string
    channelID?: string
    timeStamp?: number
}