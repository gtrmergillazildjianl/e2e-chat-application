import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-gitlab-badge',
  templateUrl: './gitlab-badge.component.html',
  styleUrls: ['./gitlab-badge.component.scss']
})
export class GitlabBadgeComponent implements OnInit {

  iconSrc = "/assets/icons/gitlab-icon.png"
  constructor() { }

  ngOnInit(): void {
  }

}
