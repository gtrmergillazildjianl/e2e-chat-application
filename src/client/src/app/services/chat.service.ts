import { Injectable } from '@angular/core';
import { Channel } from "../models/Channel";
import { Message } from "../models/Message";
import { Response } from "../models/Response"
import { EventModel } from "../models/Event";
import { ChatSession } from "../models/ChatSession";
import { HttpClient } from "@angular/common/http";
import { Observable } from 'rxjs';

import { HubConnectionBuilder, HubConnection, HubConnectionState } from "@microsoft/signalr"
import * as CryptoJS from "crypto-js";


@Injectable({
  providedIn: 'root'
})
export class ChatService {

  constructor(
    private client: HttpClient,
  ) { }

  private username = ""
  private password = ""
  private token = ""
  private channelID = ""
  private bearer = ""
  private passwordSHA256!: CryptoJS.lib.WordArray
  private hub!: HubConnection


  CreateNewChannel(password: string): Observable<Response<Channel>> {
    let challenge = this.randomString()

    return this.client.post<Response<Channel>>('/api/channels/', {
      challenge: challenge,
      token: this.computeToken(challenge, password),
    } as Channel, {
      headers: {
        "content-type": "application/json",
      }
    })
  }

  async JoinChannel(username: string, channelID: string, password: string): Promise<Response<string>> {
    let channel = await this.GetChannel(channelID).toPromise();
    if (channel.statusCode !== 200) {
      return {
        statusCode: channel.statusCode,
        message: channel.message,
      }
    }

    this.token = this.computeToken(channel.data?.challenge as string, password);
    this.username = username;
    this.password = password;
    this.passwordSHA256 = CryptoJS.SHA256(this.password)
    this.channelID = channelID;
    this.bearer = btoa(`${username}:${channelID}:${this.token}`);

    let res = await this.client.get<Response<string>>('/api/auth', {
      headers: {
        "content-type": "application/json",
        Authorization: `Bearer ${this.bearer}`,
      }
    }).toPromise()

    if (this.hub !== null && this.hub !== undefined && this.hub.state === HubConnectionState.Connected) {
      await this.hub.stop()
    }
    this.hub = new HubConnectionBuilder().withUrl('/hub/chat/').build()
    await this.hub.start()

    return res
  }

  GetChannel(channelID: string): Observable<Response<Channel>> {
    return this.client.get<Response<Channel>>(`/api/channels/${channelID}`, {
      headers: {
        "content-type": "application/json"
      }
    });
  }

  DeleteChannel(channelID: string): Observable<Response<Channel>> {
    return this.client.delete<Response<Channel>>(`/api/channels/${channelID}`, {
      headers: {
        "content-type": "application/json",
        Authorization: `Bearer ${this.bearer}`
      }
    });
  }

  CreateMessage(message: string): Observable<Response<Message>> {
    let toSend = {} as Message
    toSend.from = this.username
    toSend.channelID = this.channelID
    toSend.fourth = this.randomString()
    toSend.encryptedMessage = CryptoJS.AES.encrypt(message, this.passwordSHA256, {
      iv: CryptoJS.SHA256(toSend.fourth)
    }).toString()

    return this.client.post<Response<Message>>(`/api/channels/${this.channelID}/messages`, toSend, {
      headers: {
        "content-type": "application/json",
        Authorization: `Bearer ${this.bearer}`
      },
    });
  }

  ListenToEvents(): Observable<EventModel<any>> {
    return new Observable<EventModel<any>>(observer => {
      this.hub.on("ReceivedMessage", (event: EventModel<Message>) => {
        event.payload.encryptedMessage = CryptoJS.AES.decrypt(event.payload.encryptedMessage as string, this.passwordSHA256, {
          iv: CryptoJS.SHA256(event.payload.fourth as string)
        }).toString(CryptoJS.enc.Utf8)
        observer.next(event)
      })
    })
  }

  GetChatSession(): ChatSession {
    if (this.channelID === null || this.channelID === undefined || this.channelID === "" ||
      this.username === null || this.username === undefined || this.username === "" ||
      this.password === null || this.password === undefined || this.username === undefined) {
      throw new Error("invalid chat session")
    }
    return {
      channelID: this.channelID,
      password: this.password,
      username: this.username
    }
  }

  randomString(length = 32): string {

    let bytes = crypto.getRandomValues(new Uint8Array(length)) as any;
    return btoa(String.fromCharCode.apply(null, bytes as number[]));

  }

  private computeToken(challenge: string, password: string): string {
    return CryptoJS.HmacSHA256(challenge, password).toString(CryptoJS.enc.Base64);
  }

}
