using System;

namespace Utils.Random
{

    public static class RandomStringGenerator
    {

        public static string GenerateRandomBase64String(int byteSize)
        {
            var bytes = RandomBytes.GenerateRandomBytes(byteSize);

            return Convert.ToBase64String(bytes);
        }

        public static string GenerateRandomURLSafeBase64String(int byteSize)
        {
            return RandomStringGenerator.GenerateRandomBase64String(byteSize)
                   .TrimEnd('=').Replace('+', '-').Replace('/', '_');
        }

    }
}