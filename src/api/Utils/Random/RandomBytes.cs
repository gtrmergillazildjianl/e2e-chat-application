
using System.Security.Cryptography;

namespace Utils.Random
{

    public static class RandomBytes
    {
        public static byte[] GenerateRandomBytes(int size)
        {
            var bytes = new byte[size];
            RandomNumberGenerator.Create().GetBytes(bytes);
            return bytes;
        }
    }
}