
using Swashbuckle.AspNetCore.SwaggerGen;
using Microsoft.OpenApi.Models;
using Microsoft.AspNetCore.Authorization;
using System.Collections.Generic;
using Microsoft.Extensions.Logging;
using System;


namespace RestAPI.Middlewares.Swagger {

    public class SecurityDefinitionOperationFilter : IOperationFilter {
        
        private ILogger _logger;
        public SecurityDefinitionOperationFilter(ILogger<SecurityDefinitionOperationFilter> logger) {
            _logger = logger;
        }
        public void Apply(OpenApiOperation operation, OperationFilterContext context) {
           var methodAttributes = context.MethodInfo.GetCustomAttributes(true);
           var classAttributes = context.MethodInfo.DeclaringType.GetCustomAttributes(true);
           var attributes = new object[methodAttributes.Length + classAttributes.Length];
           methodAttributes.CopyTo(attributes, 0);
           classAttributes.CopyTo(attributes, methodAttributes.Length);

           foreach (var attribute in attributes) {
               if (attribute.GetType() == typeof(AuthorizeAttribute)) {
                   var authorizeAttribute = (AuthorizeAttribute)attribute;
                   var securityInfos = new List<string> {
                       $"{nameof(AuthorizeAttribute.Policy)}:{authorizeAttribute.Policy}",
                       $"{nameof(AuthorizeAttribute.Roles)}:{authorizeAttribute.Roles}",
                       $"{nameof(AuthorizeAttribute.AuthenticationSchemes)}:{authorizeAttribute.AuthenticationSchemes}"
                   };
                   operation.Security.Add(new OpenApiSecurityRequirement {
                       {
                           new OpenApiSecurityScheme {
                               Reference = new OpenApiReference {
                                   Id = "Bearer",
                                   Type = ReferenceType.SecurityScheme,
                               }
                           },
                           securityInfos
                       }
                   });
               }
           }
        }
    }
}