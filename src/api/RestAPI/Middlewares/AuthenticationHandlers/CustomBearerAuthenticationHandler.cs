using System.Net;
using Microsoft.AspNetCore.Authentication;
using Microsoft.Extensions.Options;
using Microsoft.Extensions.Logging;
using System.Text.Encodings.Web;
using System.Threading.Tasks;
using Microsoft.Extensions.Primitives;
using Services.Stores;
using System;
using System.Text;
using System.Security.Claims;

namespace RestAPI.Middlewares.AuthenticationHandlers {

    public class CustomBearerAuthenticationHandlerOptions : AuthenticationSchemeOptions {}
    public class CustomBearerAuthenticationHandler : AuthenticationHandler<AuthenticationSchemeOptions> {

        private IChannelStore _channelStore;
        public CustomBearerAuthenticationHandler(IOptionsMonitor<AuthenticationSchemeOptions> options, ILoggerFactory logger, UrlEncoder encoder, ISystemClock clock, IChannelStore channelStore) : base(options, logger, encoder, clock) {
            _channelStore = channelStore;
        }   

        protected override async Task<AuthenticateResult> HandleAuthenticateAsync()
        {
            
            var authorization = new StringValues();
            if (!Context.Request.Headers.TryGetValue("Authorization", out authorization)) {
                return AuthenticateResult.Fail("Missing Authorization Header");
            }

            var authHeaderArray = authorization[0].Split(" ");
            if (authHeaderArray.Length != 2) {
                return AuthenticateResult.Fail("Invalid Authorization Header");
            }

            if (authHeaderArray[0] != Scheme.Name) {
                return AuthenticateResult.Fail($"Invalid Authentication Scheme {authorization[0]}");
            }

        
            // Token Format
            //    base64 encoded string of {username}:{channelID}:{channelToken}
            var decodedToken = Encoding.UTF8.GetString(Convert.FromBase64String(authHeaderArray[1])).Split(":");
            if (decodedToken.Length != 3) {
                return AuthenticateResult.Fail("Invalid token");
            }

            if (decodedToken[0].Length < 5 || decodedToken[0].Length > 20) {
                return AuthenticateResult.Fail("username should be atleast 5 characters");
            }

            var channel = await _channelStore.GetChannel(decodedToken[1]);
            if (channel.Token != decodedToken[2]) {
                return AuthenticateResult.Fail("Wrong Token");
            }

            var claims = new[] {
                new Claim("channelID", channel.ChannelID),
                new Claim(ClaimTypes.Name, decodedToken[0])
            };
            var claimsIdentity = new ClaimsIdentity(claims, nameof(CustomBearerAuthenticationHandler));

            var ticket = new AuthenticationTicket(new ClaimsPrincipal(claimsIdentity) {
            }, this.Scheme.Name);
            return AuthenticateResult.Success(ticket);
        }
    }
}