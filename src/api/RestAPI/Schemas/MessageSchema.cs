


namespace RestAPI.Schemas {

    public class MessageSchema {

        public string EncryptedMessage { get; set; }

        public string Fourth { get; set;}

        public string From { get; set; }

        public string ChannelID { get; set; }

        public long TimeStamp { get; set;}

        
    }
}