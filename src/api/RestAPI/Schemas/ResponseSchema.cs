

namespace RestAPI.Schemas {

    public class ResponseSchema<T> {

        public int StatusCode {get; set;}

        public string Message { get; set;}

        public T Data {get; set;}

    }
}