
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using RestAPI.Schemas;

namespace RestAPI.Controllers
{

    [ApiController]
    [Route("api/[controller]")]
    public class AuthController : Controller
    {

        private ILogger _logger;


        public AuthController(ILogger<AuthController> logger)
        {
            _logger = logger;
        }

        [Authorize]
        [HttpGet]
        public ActionResult<ResponseSchema<string>> SetCookie()
        {
            var authorization = HttpContext.Request.Headers["Authorization"];
            HttpContext.Response.Cookies.Append("chat_session", authorization[0].Split(" ")[1], new CookieOptions
            {
                HttpOnly = true,
                SameSite = SameSiteMode.Lax,
                Path = "/hub/chat",
                IsEssential = true,
                Secure = true,
            });
            return new ResponseSchema<string>
            {
                Data = "success",
                Message = "success",
                StatusCode = 200
            };
        }
    }

}