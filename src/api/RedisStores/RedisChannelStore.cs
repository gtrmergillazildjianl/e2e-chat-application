
using Services.Stores;
using Services.Models;
using System;
using StackExchange.Redis;
using System.Text.Json;
using RedisStores.Exceptions;
using System.Threading.Tasks;

namespace RedisStores
{

    public class RedisChannelStore : IChannelStore
    {
        IDatabase _db;
        public RedisChannelStore(string config)
        {
            _db = ConnectionMultiplexer.Connect(config).GetDatabase();
        }

        public async Task<ChannelModel> SaveChannel(ChannelModel newChannel)
        {
            bool isSet = await _db.StringSetAsync($"channels:{newChannel.ChannelID}", JsonSerializer.Serialize(newChannel));
            if (!isSet)
            {
                throw new FailedToSetKeyException($"Failed to create channel {newChannel.ChannelID}");
            }
            return newChannel;
        }

        public async Task<ChannelModel> GetChannel(string channelID)
        {
            var exists = await _db.KeyExistsAsync($"channels:{channelID}");
            if (!exists)
            {
                return null;
            }
            var channel = _db.StringGet($"channels:{channelID}");
            if (channel.IsNullOrEmpty)
            {
                return null;
            }

            return JsonSerializer.Deserialize<ChannelModel>(channel);
        }

        public async Task<ChannelModel> DeleteChannel(string channelID)
        {
            var channel = await GetChannel(channelID);
            var isDeleted = await _db.KeyDeleteAsync($"channels:{channelID}");

            if (!isDeleted)
            {
                throw new FailedToDeleteKeyException($"Failed to delete channel {channelID}");
            }
            return channel;
        }
    }
}