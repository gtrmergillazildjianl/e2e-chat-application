
using System;

namespace RedisStores.Exceptions
{

    public class FailedToDeleteKeyException : Exception
    {
        public FailedToDeleteKeyException(string message) : base(message) { }
    }
}