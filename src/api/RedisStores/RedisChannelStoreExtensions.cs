using Microsoft.Extensions.DependencyInjection;
using Services.Stores;

namespace RedisStores
{

    public static class RedisChannelStoreExtension
    {

        public static IServiceCollection AddRedisChannelStore(this IServiceCollection service, string config)
        {
            service.AddSingleton<IChannelStore>(new RedisChannelStore(config));
            return service;
        }

    }
}