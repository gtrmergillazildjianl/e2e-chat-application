using Microsoft.Extensions.DependencyInjection;
using Services.Publishers;

namespace NSQPublishers
{

    public static class NSQEventPublisherExtensions
    {

        public static IServiceCollection AddNSQEventPublisher(this IServiceCollection services, string config)
        {
            services.AddSingleton<IEventPublisher>(new NSQEventPublisher(config));
            return services;
        }
    }
}