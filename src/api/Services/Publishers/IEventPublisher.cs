

using System.Threading.Tasks;
using Services.Models;

namespace Services.Publishers
{


    public interface IEventPublisher
    {
     
        public Task PublishEvent(EventModel<object> model);

    }
}