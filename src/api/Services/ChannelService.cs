
using Services.Stores;
using Services.Models;
using Microsoft.Extensions.Logging;
using Utils.Random;
using System.Threading.Tasks;
using System;

namespace Services
{

    public class ChannelService
    {

        private IChannelStore _store;
        private ILogger _logger;

        public ChannelService(ILogger<ChannelService> logger, IChannelStore store)
        {
            _logger = logger;
            _store = store;
        }

        public async Task<ServiceResult<ChannelModel>> CreateNewChannel(ChannelModel newChannel)
        {
            try
            {
                if (newChannel.Challenge == "" || newChannel.Challenge == null) return new ServiceResult<ChannelModel>
                {
                    Code = 400,
                    Message = "challenge is required",
                };
                var challengeBytes = Convert.FromBase64String(newChannel.Challenge);
                if (challengeBytes.Length < 32) return new ServiceResult<ChannelModel>
                {
                    Code = 400,
                    Message = "challenge should be atleast 32 bytes long"
                };

            }
            catch (FormatException)
            {
                return new ServiceResult<ChannelModel>
                {
                    Code = 400,
                    Message = "challenge is not a valid base64 string"
                };
            }

            try
            {
                if (newChannel.Token == "" || newChannel.Token == null)
                {
                    return new ServiceResult<ChannelModel>
                    {
                        Code = 400,
                        Message = "token is required"
                    };
                }
                var tokenBytes = Convert.FromBase64String(newChannel.Token);
                if (tokenBytes.Length < 32)
                {

                    return new ServiceResult<ChannelModel>
                    {
                        Code = 400,
                        Message = "token should be atleast 32 bytes long"
                    };
                }
            }
            catch (FormatException)
            {
                return new ServiceResult<ChannelModel>
                {
                    Code = 400,
                    Message = "token is not a valid base64 string"
                };
            }

            _logger.LogInformation("Creating new channel");
            newChannel.ChannelID = RandomStringGenerator.GenerateRandomURLSafeBase64String(32);
            var channel = await _store.SaveChannel(newChannel);

            return new ServiceResult<ChannelModel>
            {
                Code = 201,
                Message = "channel successfully created",
                Data = channel,
            };
        }

        public async Task<ServiceResult<ChannelModel>> GetChannel(string channelID)
        {

            var data = await _store.GetChannel(channelID);
            if (data == null)
            {
                return new ServiceResult<ChannelModel>
                {
                    Code = 404,
                    Message = "channel is not found",
                    Data = null,
                };
            }

            return new ServiceResult<ChannelModel>
            {
                Code = 200,
                Data = data,
                Message = "success",
            };
        }

        public async Task<ServiceResult<ChannelModel>> DeleteChannel(string channelID)
        {

            var data = await _store.DeleteChannel(channelID);

            if (data == null)
            {
                return new ServiceResult<ChannelModel>
                {
                    Code = 404,
                    Message = "channel is not found",
                    Data = null,
                };
            }

            return new ServiceResult<ChannelModel>
            {
                Code = 200,
                Message = "success",
                Data = data,
            };
        }
    }
}