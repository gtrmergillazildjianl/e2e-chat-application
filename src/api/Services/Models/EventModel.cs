


namespace Services.Models
{

    public static class EventTypes
    {
        public const string SendMessage = "SendMessage";
        public const string DeleteMessage = "DeleteMessage";
    }
    public class EventModel<T>
    {

        public string Type { get; set; }

        public T Payload { get; set; }

        public long TimeStamp { get; set; }

        public string ChannelID { get; set; }
    }
}