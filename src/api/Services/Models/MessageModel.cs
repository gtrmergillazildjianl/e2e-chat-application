


namespace Services.Models
{

    public class MessageModel
    {

        public string From { get; set; }

        public string EncryptedMessage { get; set; }

        public string Fourth { get; set; }

        public long TimeStamp { get; set; }

        public string ChannelID { get; set; }
    }
}