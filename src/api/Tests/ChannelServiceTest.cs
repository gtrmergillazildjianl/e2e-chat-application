using System;
using Xunit;
using Tests.MockStores;
using Services;
using Services.Models;
using Microsoft.Extensions.Logging;

namespace Tests
{

    public class ChannelServiceTest
    {

        private MockChannelStore _store;
        private ChannelService _service;
        public ChannelServiceTest()
        {
            var loggerFactory = LoggerFactory.Create(configure =>
            {
                configure.AddConsole();
            });

            _store = new MockChannelStore();
            _service = new ChannelService(loggerFactory.CreateLogger<ChannelService>(), _store);

        }

        [Theory]
        [InlineData("", "", 400, "challenge is required")]
        [InlineData("invalid*.,base64", "", 400, "challenge is not a valid base64 string")]
        [InlineData("fprd2Zx57fZxuhVpVY4qWnBc/u4=", "", 400, "challenge should be atleast 32 bytes long")]
        [InlineData("dUtV6lnKSFKcsrvJoYxOfvxCi0ZrHu2m5elBHF8msrU=", "", 400, "token is required")]
        [InlineData("dUtV6lnKSFKcsrvJoYxOfvxCi0ZrHu2m5elBHF8msrU=", "invalid.,*base64", 400, "token is not a valid base64 string")]
        [InlineData("dUtV6lnKSFKcsrvJoYxOfvxCi0ZrHu2m5elBHF8msrU=", "fprd2Zx57fZxuhVpVY4qWnBc/u4=", 400, "token should be atleast 32 bytes long")]
        [InlineData("dUtV6lnKSFKcsrvJoYxOfvxCi0ZrHu2m5elBHF8msrU=", "dUtV6lnKSFKcsrvJoYxOfvxCi0ZrHu2m5elBHF8msrU=", 201, "channel successfully created")]
        public async void TestCreateChannel(string challenge, string token, int code, string message)
        {
            var result = await _service.CreateNewChannel(new ChannelModel
            {
                Challenge = challenge,
                Token = token,
            });
            Assert.Equal(code, result.Code);
            Assert.Equal(message, result.Message);
            if (code == 201)
            {
                Assert.NotNull(await _store.GetChannel(result.Data.ChannelID));
            }
            else
            {
                Assert.Null(result.Data);
            }
        }

        [Theory]
        [InlineData("abcdefghijklmnopqrstuvwqyx1", 200, "success")]
        [InlineData("not-existing-channel", 404, "channel is not found")]
        public async void TestGetChannel(string channelID, int code, string message)
        {
            var result = await _service.GetChannel(channelID);
            Assert.Equal(code, result.Code);
            Assert.Equal(message, result.Message);
            Assert.Equal(await _store.GetChannel(channelID), result.Data);
        }

        [Theory]
        [InlineData("abcdefghijklmnopqrstuvwqyx1", 200, "success")]
        [InlineData("not-existing-channel", 404, "channel is not found")]
        public async void TestDeleteChannel(string channelID, int code, string message)
        {
            var result = await _service.DeleteChannel(channelID);
            Assert.Equal(code, result.Code);
            Assert.Equal(message, result.Message);
            Assert.Null(await _store.GetChannel(channelID));

        }


    }
}