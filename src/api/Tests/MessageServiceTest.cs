using Services;
using Tests.MockPublishers;
using Microsoft.Extensions.Logging;
using Services.Models;
using Xunit;

namespace Tests
{

    public class MessageServiceTest
    {


        private MessageService _service;
        private MockEventPublisher _publisher;

        public MessageServiceTest()
        {
            var loggerFactory = LoggerFactory.Create(config =>
            {
                config.AddConsole();
            });
            _publisher = new MockEventPublisher();
            _service = new MessageService(loggerFactory.CreateLogger<MessageService>(), _publisher);
        }

        [Theory]
        [InlineData("", "", "", 400, "from is required")]
        [InlineData("a", "", "", 400, "from should be atleast 5 characters")]
        [InlineData("zildjianzildjianzzzzzzzzzzzzz", "", "", 400, "from should be atmost 20 characters")]
        [InlineData("zildjian", "", "", 400, "encryptedMessage is required")]
        [InlineData("zildjian", "invalid.*base64/", "", 400, "encryptedMessage is not a valid base64 string")]
        [InlineData("zildjian", "dUtV6lnKSFKcsrvJoYxOfvxCi0ZrHu2m5elBHF8msrU=", "", 400, "fourth is required")]
        [InlineData("zildjian", "dUtV6lnKSFKcsrvJoYxOfvxCi0ZrHu2m5elBHF8msrU=", "invalid.*base64/", 400, "fourth is not a valid base64 string")]
        [InlineData("zildjian", "dUtV6lnKSFKcsrvJoYxOfvxCi0ZrHu2m5elBHF8msrU=", "fprd2Zx57fZxuhVpVY4qWnBc/u4=", 400, "fourth should be atleast 32 bytes in length")]
        [InlineData("zildjian", "dUtV6lnKSFKcsrvJoYxOfvxCi0ZrHu2m5elBHF8msrU=", "dUtV6lnKSFKcsrvJoYxOfvxCi0ZrHu2m5elBHF8msrU=", 201, "success")]
        public async void TestSendMessage(string from, string encryptedMessage, string fourth, int code, string message)
        {
            _publisher.ResetState();
            var result = await _service.SendMessage(new MessageModel
            {
                ChannelID = "some-channel-id",
                EncryptedMessage = encryptedMessage,
                Fourth = fourth,
                From = from,
            });
            Assert.Equal(code, result.Code);
            Assert.Equal(message, result.Message);
            if (code == 201)
            {
                Assert.NotNull(await _publisher.GetPublishedEvent("some-channel-id"));
            }

        }

    }
}