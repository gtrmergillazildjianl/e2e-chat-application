using Services.Publishers;
using System.Collections.Generic;
using Services.Models;
using System.Threading.Tasks;


namespace Tests.MockPublishers
{

    public class MockEventPublisher : IEventPublisher
    {

        private Dictionary<string, EventModel<object>> _state;


        public MockEventPublisher()
        {
            _state = new Dictionary<string, EventModel<object>>();
        }

        public void ResetState()
        {
            _state.Clear();
        }

        public Task PublishEvent(EventModel<object> model)
        {
            return Task.Run(() =>
            {
                _state.Add(model.ChannelID, model);
            });
        }

        public Task<EventModel<object>> GetPublishedEvent(string channelID)
        {
            return Task.Run<EventModel<object>>(() =>
            {
                try
                {
                    return _state[channelID];
                }
                catch (KeyNotFoundException)
                {
                    return null;
                }
            });
        }


    }
}