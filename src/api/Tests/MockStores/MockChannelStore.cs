
using Services.Stores;
using Services.Models;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace Tests.MockStores
{
    public class MockChannelStore : IChannelStore
    {

        private Dictionary<string, ChannelModel> _state;

        public MockChannelStore()
        {
            _state = new Dictionary<string, ChannelModel>();
            ResetState();
        }

        public void ResetState()
        {
            _state["abcdefghijklmnopqrstuvwqyx1"] = new ChannelModel
            {
                Challenge = "samplechallenge1",
                ChannelID = "abcdefghijklmnopqrstuvwqyx1",
                Token = "sampletoken1",
            };
            _state["abcdefghijklmnopqrstuvwqyx2"] = new ChannelModel
            {
                Challenge = "samplechallenge2",
                ChannelID = "abcdefghijklmnopqrstuvwqyx2",
                Token = "sampletoken2",
            };
            _state["abcdefghijklmnopqrstuvwqyx3"] = new ChannelModel
            {
                Challenge = "samplechallenge",
                ChannelID = "abcdefghijklmnopqrstuvwqyx3",
                Token = "sampletoken2",
            };
        }
        public Task<ChannelModel> SaveChannel(ChannelModel newChannel)
        {
            return Task.Run<ChannelModel>(() =>
            {
                return _state[newChannel.ChannelID] = newChannel;
            });
        }

        public Task<ChannelModel> GetChannel(string channelID)
        {
            return Task.Run<ChannelModel>(() =>
            {
                try
                {
                    return _state[channelID];
                }
                catch (KeyNotFoundException)
                {
                    return null;
                }
            });
        }

        public Task<ChannelModel> DeleteChannel(string channelID)
        {
            return Task.Run<ChannelModel>(() =>
            {
                ChannelModel current;
                try
                {
                    current = _state[channelID];
                }
                catch (KeyNotFoundException)
                {
                    return null;
                }

                var channel = new ChannelModel
                {
                    Challenge = current.Challenge,
                    ChannelID = current.ChannelID,
                    Token = current.Token
                };
                _state.Remove(channelID);
                return channel;
            });
        }
    }
}